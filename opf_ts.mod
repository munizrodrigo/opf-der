# ------------- OPF-TS MODEL ------------- #

# ---------------- Grid ------------------ #

param grid_name symbolic; # name of the grid

param n_branch default Infinity; # number of branches

set BUS; # set of buses
set BRANCH within {1..n_branch} cross BUS cross BUS; # set of branches

# ---------------- Files ----------------- #

param grid_out_filename default "opf_ts_out.txt" symbolic;
model "ampl\\model\\grid_files_param.mod";

# -------------- Constants --------------- #

param pi := 4 * atan(1);

# -------- Objectives Parameters --------- #

param w default 0.0;

# --------------- Bus Data --------------- #

model "ampl\\model\\bus_param.mod";

# ------------- Branch Data -------------- #

model "ampl\\model\\branch_param.mod";

# -------------- Variables --------------- #

var bus_voltage {i in BUS} >= bus_voltage_min[i], <= bus_voltage_max[i]; 
var bus_b_shunt {i in BUS} >= bus_b_shunt_min[i], <= bus_b_shunt_max[i];
var bus_angle {i in BUS};
var branch_tap {(l,k,m) in BRANCH} >= branch_tap_min[l,k,m], <= branch_tap_max[l,k,m]; 
var branch_def {(l,k,m) in BRANCH} >= branch_def_min[l,k,m], <= branch_def_max[l,k,m];

# Control adjustments variables
var bus_voltage_adjustment {i in BUS: bus_type[i] == 2 || bus_type[i] == 3} binary; # s1
var branch_tap_adjustment {(l,k,m) in BRANCH: branch_type[l,k,m] == 1 || branch_type[l,k,m] == 2} binary; # s2
var bus_b_shunt_adjustment {i in BUS: bus_b_dispatch[i] != 0} binary; # s3
var total_control_adjustment = sum {i in BUS: bus_type[i] == 2 || bus_type[i] == 3} bus_voltage_adjustment[i] + sum {(l,k,m) in BRANCH: branch_type[l,k,m] == 1 || branch_type[l,k,m] == 2} branch_tap_adjustment[l,k,m] + sum {i in BUS: bus_b_dispatch[i] != 0} bus_b_shunt_adjustment[i];

# --------- Auxiliary Variables ---------- #

var p_g {BUS}; # final active power generation
var q_g {BUS}; # final reactive power generation

var p_d {BRANCH}; # final active direct flow
var q_d {BRANCH}; # final reactive direct flow
var p_r {BRANCH}; # final active reverse flow
var q_r {BRANCH}; # final reactive reverse flow

# ----------- System Matrix -------------- #

model "ampl\\model\\system_matrix.mod";

# ------ Auxiliary Generation Data ------- #

# TODO verify inductive and capacitive generation equations

var reactive_generation =  sum {k in BUS} abs(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))));

var inductive_generation = sum {k in BUS} min(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))), 0);

var capacitive_generation =  sum {k in BUS} max(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k]-bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))), 0);

# --------------- Losses ----------------- #

var total_loss = sum {(l,k,m) in BRANCH} (branch_g[l,k,m] * (bus_voltage[k]^2 * branch_tap[l,k,m]^2 + bus_voltage[m]^2 - 2 * bus_voltage[k] * bus_voltage[m] * branch_tap[l,k,m] * cos(bus_angle[k] - bus_angle[m])));

# ------- Normalization Parameters ------- #

param min_total_loss default 0;
param max_total_loss default 1;
param min_total_control_adjustment default 0;
param max_total_control_adjustment default 1;

# --------- Normalized Functions --------- #

var norm_total_loss = (total_loss - min_total_loss) / (max_total_loss - min_total_loss);

var norm_total_control_adjustment = (total_control_adjustment - min_total_control_adjustment) / (max_total_control_adjustment - min_total_control_adjustment);

# ------------- Objectives --------------- #

minimize losses: total_loss;

minimize control_adjustment: total_control_adjustment;

minimize losses_and_control: w * norm_total_loss + (1 - w) * norm_total_control_adjustment;

# ------------- Constraints -------------- #

subject to p_load {k in BUS: bus_type[k] == 0}: bus_p_gen[k] - bus_p_load[k] - sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * cos(bus_angle[k] - bus_angle[m]) + B[k,m] * sin(bus_angle[k] - bus_angle[m]))) = 0;

subject to q_load {k in BUS: bus_type[k] == 0}: bus_q_gen[k] - bus_q_load[k] - sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))) = 0;

subject to p_inj {k in BUS: bus_type[k] == 2}: bus_p_gen_min[k] * bus_p_gen[k] <= bus_p_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * cos(bus_angle[k] - bus_angle[m]) + B[k,m] * sin(bus_angle[k] - bus_angle[m]))) <= bus_p_gen_max[k] * bus_p_gen[k];

subject to q_inj {k in BUS: bus_type[k] == 2 || bus_type[k] == 3}: bus_q_gen_min[k] <= bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))) <= bus_q_gen_max[k];
               
# Control adjustments variables constraints

subject to voltage_adjustment_min {k in BUS: bus_type[k] == 2 || bus_type[k] == 3}: bus_voltage_adjustment[k] * (bus_voltage_min[k] - bus_voltage0[k]) <= bus_voltage[k] - bus_voltage0[k];

subject to voltage_adjustment_max {k in BUS: bus_type[k] == 2 || bus_type[k] == 3}: bus_voltage[k] - bus_voltage0[k] <= bus_voltage_adjustment[k] * (bus_voltage_max[k] - bus_voltage0[k]);

subject to tap_adjustment_min {(l,k,m) in BRANCH: branch_type[l,k,m] == 1 || branch_type[l,k,m] == 2}: branch_tap_adjustment[l,k,m] * (branch_tap_min[l,k,m] - branch_tap0[l,k,m]) <= branch_tap[l,k,m] - branch_tap0[l,k,m];

subject to tap_adjustment_max {(l,k,m) in BRANCH: branch_type[l,k,m] == 1 || branch_type[l,k,m] == 2}: branch_tap[l,k,m] - branch_tap0[l,k,m] <= branch_tap_adjustment[l,k,m] * (branch_tap_max[l,k,m] - branch_tap0[l,k,m]);

subject to b_shunt_adjustment_min {k in BUS: bus_b_dispatch[k] != 0}: bus_b_shunt_adjustment[k] * (bus_b_shunt_min[k] - bus_b_shunt0[k]) <= bus_b_shunt[k] - bus_b_shunt0[k];

subject to b_shunt_adjustment_max {k in BUS: bus_b_dispatch[k] != 0}: bus_b_shunt[k] - bus_b_shunt0[k] <= bus_b_shunt_adjustment[k] * (bus_b_shunt_max[k] - bus_b_shunt0[k]);