from os.path import abspath
from time import time
from amplpy import AMPL, OutputHandler, ErrorHandler
from tabulate import tabulate
from math import sqrt


class AMPLOutput(OutputHandler):
    def output(self, kind, msg):
        pass


class AMPLError(ErrorHandler):
    def error(self, exception):
        print("Error:", exception.get_message())

    def warning(self, exception):
        print("Warning:", exception.get_message())


def main():
    ampl.option["solver"] = "bonmin"
    ampl.set_option("bonmin_options", "bonmin.bb_log_level 0 bonmin.nlp_log_level 0")

    ampl.read("dopf_misocp.mod")
    ampl.read_data("dopf_misocp.dat")

    ampl.eval("objective losses;")

    m = 1
    e = 0.02
    l = 0.02

    while True:
        ampl.solve()

        bus_voltage_square = ampl.var["bus_voltage_square"].get_values().to_dict()
        bus_voltage = {key: sqrt(value) for key, value in bus_voltage_square.items()}
        branch_tap = ampl.var["branch_tap"].get_values().to_dict()[2.0, 2.0, 3.0]
        step_size = ampl.param["branch_step_size"].get_values().to_dict()[2.0, 2.0, 3.0]

        if abs(bus_voltage_square[3.0] - bus_voltage_square[2.0] * (1 + step_size * branch_tap) ** 2) <= e:
            print("atingiu a tolerancia")
            print(m)
            print(bus_voltage)
            print(branch_tap)
            print(abs(bus_voltage_square[3.0] - bus_voltage_square[2.0] * (1 + step_size * branch_tap) ** 2))
            break

        bus_voltage_square_mccormick_upper = bus_voltage_square[2.0] + l
        bus_voltage_square_mccormick_lower = bus_voltage_square[2.0] - l

        branch_ltc_k2_mccormick_upper = 1 / (bus_voltage_square[3] - l)
        branch_ltc_k2_mccormick_lower = 1 / (bus_voltage_square[3] + l)

        ampl.param["bus_voltage_square_mccormick_upper"][2.0] = bus_voltage_square_mccormick_upper
        ampl.param["bus_voltage_square_mccormick_lower"][2.0] = bus_voltage_square_mccormick_lower
        ampl.param["branch_ltc_k2_mccormick_upper"][2.0, 2.0, 3.0] = branch_ltc_k2_mccormick_upper
        ampl.param["branch_ltc_k2_mccormick_lower"][2.0, 2.0, 3.0] = branch_ltc_k2_mccormick_lower

        print(m)
        print(bus_voltage)
        print(branch_tap)
        print(abs(bus_voltage_square[3.0] - bus_voltage_square[2.0] * (1 + step_size * branch_tap) ** 2))
        # print(bus_voltage_square_mccormick_lower, bus_voltage_square_mccormick_upper, branch_ltc_k2_mccormick_lower, branch_ltc_k2_mccormick_upper)

        m += 1
        l /= 10


if __name__ == "__main__":
    try:
        ampl = AMPL()

        output_handler = AMPLOutput()
        ampl.set_output_handler(output_handler)

        error_handler = AMPLError()
        ampl.set_error_handler(error_handler)

        main()

    except Exception as exc:
        raise exc
