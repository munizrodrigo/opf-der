set YBUS := setof {i in BUS} (i,i) union setof {(l,k,m) in BRANCH} (k,m) union setof {(l,k,m) in BRANCH} (m,k);

# TODO verify tap modelling
# TODO verify phase-shift impact on G and B matrix

var G {(k,m) in YBUS} =
if (k == m) then (bus_g_shunt[k] + sum {(l,k,i) in BRANCH} branch_g[l,k,i] * branch_tap[l,k,i]^2
                                + sum {(l,i,k) in BRANCH} branch_g[l,i,k])
else if (k != m) then (sum {(l,k,m) in BRANCH} (- branch_g[l,k,m] * cos(branch_def[l,k,m]) - branch_b[l,k,m] * sin(branch_def[l,k,m])) * branch_tap[l,k,m]
                     +sum {(l,m,k) in BRANCH} (- branch_g[l,m,k] * cos(branch_def[l,m,k]) + branch_b[l,m,k] * sin(branch_def[l,m,k])) * branch_tap[l,m,k]);

var B {(k,m) in YBUS} =
if (k == m) then (bus_b_shunt[k] + sum {(l,k,i) in BRANCH} (branch_b[l,k,i] * branch_tap[l,k,i]^2 + branch_bsh[l,k,i] / 2)
                                + sum {(l,i,k) in BRANCH} (branch_b[l,i,k] + branch_bsh[l,i,k] / 2))
else if (k != m) then (sum {(l,k,m) in BRANCH} (branch_g[l,k,m] * sin(branch_def[l,k,m]) - branch_b[l,k,m] * cos(branch_def[l,k,m])) * branch_tap[l,k,m]
                     +sum {(l,m,k) in BRANCH} (-branch_g[l,m,k] * sin(branch_def[l,m,k]) - branch_b[l,m,k] * cos(branch_def[l,m,k])) * branch_tap[l,m,k]);