param branch_type {BRANCH};
param branch_r {BRANCH};
param branch_x {BRANCH};
param branch_bsh {BRANCH};
param branch_tap0 {BRANCH};
param branch_tap_min {BRANCH};
param branch_tap_max {BRANCH};
param branch_step_size {BRANCH};
param branch_def0 {BRANCH};
param branch_def_min {BRANCH};
param branch_def_max {BRANCH};
param branch_current_max {BRANCH};
param branch_g {(l,k,m) in BRANCH} := branch_r[l,k,m] / (branch_r[l,k,m]^2 + branch_x[l,k,m]^2);
param branch_b {(l,k,m) in BRANCH} := -branch_x[l,k,m] / (branch_r[l,k,m]^2 + branch_x[l,k,m]^2);