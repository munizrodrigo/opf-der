from os.path import abspath, join, dirname
from yaml import safe_load
from pandas import DataFrame as PandasFrame
from amplpy import AMPL, OutputHandler, ErrorHandler, DataFrame as AMPLFrame
from tabulate import tabulate
from opf_der.grid import Grid
from opf_der.opf_model import DOPFCR


class AMPLOutput(OutputHandler):
    def output(self, kind, msg):
        print(msg)


class AMPLError(ErrorHandler):
    def error(self, exception):
        print("Error:", exception.get_message())

    def warning(self, exception):
        print("Warning:", exception.get_message())


def create_grid_dataframes(bus_dict, branch_dict):
    bus_df = PandasFrame.from_dict(bus_dict, orient="index")
    bus_df = AMPLFrame.from_pandas(bus_df, index_names=["BUS"])

    branch_df = PandasFrame.from_dict(branch_dict, orient="index")
    branch_df = branch_df.set_index(["bus_from", "bus_to"])
    branch_df = AMPLFrame.from_pandas(branch_df, index_names=["bus_from", "bus_to"])

    return bus_df, branch_df


def main():
    grid_name = "ieee4-simplest"
    grid_dir = join(dirname(abspath(__file__)), "grid")
    grid_path = join(grid_dir, grid_name)
    grid_file = join(grid_path, f"{grid_name}.yml")

    with open(grid_file, "r") as yml_file:
        grid_dict = safe_load(yml_file)

    grid = Grid(grid_dict=grid_dict)

    bus_dict = grid.create_ampl_bus_dict()
    branch_dict = grid.create_ampl_branch_dict()

    bus_df, branch_df = create_grid_dataframes(bus_dict=bus_dict, branch_dict=branch_dict)

    dopf_misocp = DOPFCR(ampl=ampl, bus_df=bus_df, branch_df=branch_df, grid=grid)
    dopf_misocp.generate_model()
    dopf_misocp.set_data()

    dopf_misocp.export_model()
    dopf_misocp.export_data()

    dopf_misocp.solve()

    bus_df_solved, branch_df_solved = dopf_misocp.get_output_dataframe()

    print(bus_df_solved)
    print(branch_df_solved)

    grid_out_file = join(grid_path, f"{grid_name}_out.txt")

    with open(grid_out_file, "w") as out_file:
        out_file.write(tabulate(bus_df_solved, headers="keys", tablefmt="grid", floatfmt=".4f"))

    grid_out_file = join(grid_path, f"{grid_name}_out.md")

    with open(grid_out_file, "w") as out_file:
        out_file.write(tabulate(bus_df_solved, headers="keys", tablefmt="github", floatfmt=".4f"))


if __name__ == "__main__":
    try:
        ampl = AMPL()

        output_handler = AMPLOutput()
        ampl.set_output_handler(output_handler)

        error_handler = AMPLError()
        ampl.set_error_handler(error_handler)

        main()

    except Exception as unknown_exception:
        raise unknown_exception
