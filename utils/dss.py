from py_dss_interface import DSSDLL
from asteval import Interpreter, make_symbol_table


symtable = make_symbol_table(use_numpy=True, wye="wye", delta="delta", y=True, yes=True, n=False, no=False)
aeval = Interpreter(symtable=symtable)


class DSSObject(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return str(self.__dict__)


class Load(DSSObject):
    def __init__(self, name, bus1, kv, kw, kvar, kva, pf, vminpu, vmaxpu):
        super(Load, self).__init__(name=name)
        self.bus1 = bus1
        self.kv = kv
        self.kw = kw
        self.kvar = kvar
        self.kva = kva
        self.pf = pf
        self.vminpu = vminpu
        self.vmaxpu = vmaxpu


class Capacitor(DSSObject):
    def __init__(self, name, bus1, kv, kvar):
        super(Capacitor, self).__init__(name=name)
        self.bus1 = bus1
        self.kv = kv
        self.kvar = kvar


class Line(DSSObject):
    def __init__(self, name, bus1, bus2, length, r1, x1, c1, switch):
        super(Line, self).__init__(name=name)
        self.bus1 = bus1
        self.bus2 = bus2
        self.length = length
        self.r1 = r1
        self.x1 = x1
        self.c1 = c1
        self.switch = switch


class Transformer(DSSObject):
    def __init__(self, name, buses, conns, kvs, kvas, taps, min_tap, max_tap, num_taps, r_percent, xhl_percent):
        super(Transformer, self).__init__(name=name)
        self.buses = buses
        self.conns = conns
        self.kvs = kvs
        self.kvas = kvas
        self.taps = taps
        self.min_tap = min_tap
        self.max_tap = max_tap
        self.num_taps = num_taps
        self.r_pu = [r / 100 for r in r_percent]
        self.xhl_pu = xhl_percent / 100


class VSource(DSSObject):
    def __init__(self, name, bus1, basekv, angle, r1, x1):
        super(VSource, self).__init__(name=name)
        self.bus1 = bus1
        self.basekv = basekv
        self.angle = angle
        self.r1 = r1
        self.x1 = x1


class Generator(DSSObject):
    def __init__(self, name, bus1, kv, kw, kvar, kva, pf, maxkvar, minkvar, vminpu, vmaxpu):
        super(Generator, self).__init__(name=name)
        self.bus1 = bus1
        self.kv = kv
        self.kw = kw
        self.kvar = kvar
        self.kva = kva
        self.pf = pf
        self.maxkvar = maxkvar
        self.minkvar = minkvar
        self.vminpu = vminpu
        self.vmaxpu = vmaxpu


class DSS(DSSDLL):
    def __init__(self, dll_folder_param=None):
        super().__init__(dll_folder_param=dll_folder_param)
        self.__loads = []
        self.__capacitors = []
        self.__lines = []
        self.__transformers = []
        self.__vsources = []
        self.__generators = []

    def buses(self):
        return [(number + 1, name) for number, name in enumerate(self.circuit_all_bus_names())]

    def buses_by_name(self):
        return {name: number + 1 for number, name in enumerate(self.circuit_all_bus_names())}

    def loads(self):
        self.__loads = []
        if self.loads_first() != 0:
            for load in range(self.loads_count()):
                load_obj = Load(
                    name=self.loads_read_name(),
                    bus1=self.cktelement_read_bus_names()[0],
                    kv=self.loads_read_kv(),
                    kw=self.loads_read_kw(),
                    kvar=self.loads_read_kvar(),
                    kva=self.loads_read_kva(),
                    pf=self.loads_read_pf(),
                    vminpu=self.loads_read_vmin_pu(),
                    vmaxpu=self.loads_read_vmax_pu()
                )
                self.__loads.append(load_obj)
                self.loads_next()
        return self.__loads

    def capacitors(self):
        self.__capacitors = []
        if self.capacitors_first() != 0:
            for capacitor in range(self.capacitors_count()):
                cap_obj = Capacitor(
                    name=self.capacitors_read_name(),
                    bus1=self.cktelement_read_bus_names()[0],
                    kv=self.capacitors_read_kv(),
                    kvar=self.capacitors_read_kvar()
                )
                self.__capacitors.append(cap_obj)
                self.capacitors_next()
        return self.__capacitors

    def lines(self):
        self.__lines = []
        if self.lines_first() != 0:
            for line in range(self.lines_count()):
                line_obj = Line(
                    name=self.lines_read_name(),
                    bus1=self.cktelement_read_bus_names()[0],
                    bus2=self.cktelement_read_bus_names()[1],
                    length=self.lines_read_length(),
                    r1=self.lines_read_r1(),
                    x1=self.lines_read_x1(),
                    c1=self.lines_read_c1(),
                    switch=aeval(self.dssproperties_read_value("15"))
                )
                self.__lines.append(line_obj)
                self.lines_next()
        return self.__lines

    def transformers(self):
        self.__transformers = []
        if self.transformers_first() != 0:
            for transformer in range(self.transformers_count()):
                transformer_obj = Transformer(
                    name=self.transformers_read_name(),
                    buses=self.cktelement_read_bus_names(),
                    conns=aeval(self.dssproperties_read_value("13")),
                    kvs=aeval(self.dssproperties_read_value("14")),
                    kvas=aeval(self.dssproperties_read_value("15")),
                    taps=aeval(self.dssproperties_read_value("16")),
                    min_tap=self.transformers_read_min_tap(),
                    max_tap=self.transformers_read_max_tap(),
                    num_taps=self.transformers_read_num_taps(),
                    r_percent=aeval(self.dssproperties_read_value("37")),
                    xhl_percent=self.transformers_read_xhl()
                )
                self.__transformers.append(transformer_obj)
                self.transformers_next()
        return self.__transformers

    def vsources(self):
        self.__vsources = []
        if self.vsources_first() != 0:
            for vsource in range(self.vsources_count()):
                vsource_obj = VSource(
                    name=self.vsources_read_name(),
                    bus1=self.cktelement_read_bus_names()[0],
                    basekv=self.vsources_read_base_kv(),
                    angle=self.vsources_read_angle_deg(),
                    r1=aeval(self.dssproperties_read_value("13")),
                    x1=aeval(self.dssproperties_read_value("14"))
                )
                self.__vsources.append(vsource_obj)
                self.vsources_next()
        return self.__vsources

    def generators(self):
        self.__generators = []
        if self.generators_first() != 0:
            for generator in range(self.generators_count()):
                generator_obj = Generator(
                    name=self.generators_read_name(),
                    bus1=self.cktelement_read_bus_names()[0],
                    kv=self.generators_read_kv(),
                    kw=self.generators_read_kw(),
                    kvar=self.generators_read_kvar(),
                    kva=self.generators_read_kva_rated(),
                    pf=self.generators_read_pf(),
                    maxkvar=aeval(self.dssproperties_read_value("21")),
                    minkvar=aeval(self.dssproperties_read_value("22")),
                    vminpu=self.generators_read_vmin_pu(),
                    vmaxpu=self.generators_read_vmax_pu()
                )
                self.__generators.append(generator_obj)
                self.generators_next()
        return self.__generators

    def get_voltages_pu(self, single_phase=False):
        if single_phase:
            bus_names = self.circuit_all_bus_names()
        else:
            bus_names = self.circuit_all_node_names()
        bus_voltage = self.circuit_all_bus_vmag_pu()
        voltage_pu = {name: voltage for name, voltage in zip(bus_names, bus_voltage)}
        return voltage_pu
