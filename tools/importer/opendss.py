# -*- coding: utf-8 -*-
"""
This module will try to convert OpenDSS grids to this project modelling format.

.. codeauthor:: João Rodrigo Muniz <joao.muniz@usp.br>

Example
-------
Just run it. It will change in the future.
"""

from os.path import abspath, join
from tempfile import TemporaryDirectory
from collections import defaultdict
from tabulate import tabulate
from math import pi
from re import sub
import PySimpleGUI as Gui
from utils.dss import DSS


if __name__ == "__main__":
    title = "OPF-DER - OpenDSS Importer"

    dss_master_path = Gui.popup_get_file(
        message="Enter the grid OpenDSS master file:",
        title=title
    )

    try:
        S_base_mva = float(Gui.popup_get_text(message="Insert the grid base MVA:", title=title))
    except ValueError:
        Gui.popup_error(
            "The specified base MVA was not a valid number. The default value of 100 MVA will be used instead!",
            title=title
        )
        S_base_mva = 100

    try:
        frequency = float(Gui.popup_get_text(message="Insert the grid nominal frequency (Hz):", title=title))
    except ValueError:
        Gui.popup_error(
            "The specified frequency was not a valid number. The default value of 60 Hz will be used instead!",
            title=title
        )
        frequency = 60.0

    output_path = Gui.popup_get_folder(
        message="Enter the desired output directory:",
        title=title
    )

    if dss_master_path == "" or output_path == "":
        Gui.popup_error(
            "One of the needed path is missing. Exiting now...",
            title=title
        )
        exit()

    dss_master_path = abspath(dss_master_path)
    output_path = abspath(output_path)

    dss = DSS()
    dss.dss_write_allow_forms(1)

    # Read the original grid and supress any solve or show command
    with open(dss_master_path, "r") as file:
        original_file = file.read()

    modified_file = str(original_file)
    modified_file = modified_file.replace("solve", "!solve")
    modified_file = modified_file.replace("Solve", "!Solve")
    modified_file = modified_file.replace("show", "!show")
    modified_file = modified_file.replace("Show", "!Show")

    with open(dss_master_path, "w") as file:
        file.write(modified_file)

    try:
        dss.text(f"compile [{dss_master_path}]")
        dss.solution_solve()
        dss.text("MakePosSeq")
    except UnicodeError:
        Gui.popup_error(
            ("The selected .dss file is on an invalid path. Usually this is due a non-ascii character on the file "
             "path. Try moving this file to another directory without whitespaces or accents."),
            title=title
        )
        exit()

    # Revert the dss file to original version
    with open(dss_master_path, "w") as file:
        file.write(original_file)

    # Create a temporary directory with the positive sequence equivalent
    with TemporaryDirectory() as tmp_dir:
        dss.text(f"Save Circuit Dir=[{tmp_dir}]")

        dss.dss_clear_all()

        tmp_master_dss = abspath(join(tmp_dir, "Master.dss"))

        # Remove MakePosSeq inconsistencies
        with open(abspath(join(tmp_dir, "Line.dss")), "r") as file:
            original_seqpos_file = file.read()
            modified_seqpos_file = sub(r"spacing=(\S*)", r"spacing='\1'", original_seqpos_file)
            modified_seqpos_file = sub(r"wires=(\S*)", r"wires='\1'", modified_seqpos_file)
            modified_seqpos_file = sub(r"cncables=(\S*)", r"cncables='\1'", modified_seqpos_file)

        with open(abspath(join(tmp_dir, "Line.dss")), "w") as file:
            file.write(modified_seqpos_file)

        dss.text(f"redirect [{tmp_master_dss}]")

        # Solve in snapshot mode to get initial voltages
        dss.solution_write_mode(0)
        dss.solution_solve()
        bus_voltages = dss.get_voltages_pu(single_phase=True)

        # Reset grid to its original version
        dss.dss_clear_all()
        dss.text(f"redirect [{tmp_master_dss}]")

        # Define source bus
        try:
            sourcebus = list(filter(lambda s: s.bus1 == "sourcebus", dss.vsources()))[0]
        except IndexError:
            sourcebus = dss.vsources()[0]

        # Define pu bases
        V_base_kv = sourcebus.basekv
        Z_base_ohm = V_base_kv ** 2 / S_base_mva
        Y_base_ohm = 1 / Z_base_ohm

        # Generate bus file
        with open(abspath(join(output_path, "bus.txt")), "w") as file:
            bus_dict = defaultdict(list)

            for bus_index, bus_name in dss.buses():
                loads = list(filter(lambda l: l.bus1 == bus_name, dss.loads()))
                capacitors = list(filter(lambda c: c.bus1 == bus_name, dss.capacitors()))
                vsources = list(filter(lambda s: s.bus1 == bus_name, dss.vsources()))
                generators = list(filter(lambda g: g.bus1 == bus_name, dss.generators()))

                if vsources:
                    bus_type = 3
                    bus_angle0 = vsources[0].angle
                    bus_p_gen = 1.0
                    bus_q_gen = 0.0
                    bus_q_min = -99.9999
                    bus_q_max = 99.9999
                else:
                    if generators:
                        bus_type = 2
                    else:
                        bus_type = 0

                    bus_angle0 = 0.0
                    bus_p_gen = (sum(generator.kw for generator in generators) / 1000) / S_base_mva
                    bus_q_gen = (sum(generator.kvar for generator in generators) / 1000) / S_base_mva
                    bus_q_min = (sum(generator.minkvar for generator in generators) / 1000) / S_base_mva
                    bus_q_max = (sum(generator.maxkvar for generator in generators) / 1000) / S_base_mva

                bus_p_load = (sum(load.kw for load in loads) / 1000) / S_base_mva
                bus_q_load = (sum(load.kvar for load in loads) / 1000) / S_base_mva

                try:
                    bus_voltage_min = max(
                        [load.vminpu for load in loads] + [generator.vminpu for generator in generators]
                    )
                except ValueError:
                    bus_voltage_min = 0.95

                try:
                    bus_voltage_max = min(
                        [load.vmaxpu for load in loads] + [generator.vmaxpu for generator in generators]
                    )
                except ValueError:
                    bus_voltage_max = 1.05

                bus_voltage0 = bus_voltages[bus_name]

                bus_x_shunt = [(capacitor.kv ** 2) / (capacitor.kvar / 1000) for capacitor in capacitors]
                bus_x_shunt = [x / Z_base_ohm for x in bus_x_shunt]
                bus_b_shunt0 = float(sum([1 / x for x in bus_x_shunt]))

                bus_dict["BUS"].append(bus_index)
                bus_dict["bus_type"].append(bus_type)
                bus_dict["bus_name"].append("\"" + bus_name + "\"")
                bus_dict["bus_angle0"].append(bus_angle0)
                bus_dict["bus_voltage0"].append(bus_voltage0)
                bus_dict["bus_voltage_min"].append(bus_voltage_min)
                bus_dict["bus_voltage_max"].append(bus_voltage_max)
                bus_dict["bus_p_gen"].append(bus_p_gen)
                bus_dict["bus_p_gen_min"].append(0.9)
                bus_dict["bus_p_gen_max"].append(1.1)
                bus_dict["bus_q_gen"].append(bus_q_gen)
                bus_dict["bus_q_gen_min"].append(bus_q_min)
                bus_dict["bus_q_gen_max"].append(bus_q_max)
                bus_dict["bus_p_load"].append(bus_p_load)
                bus_dict["bus_q_load"].append(bus_q_load)
                bus_dict["bus_g_shunt"].append(0.0)
                bus_dict["bus_b_shunt0"].append(bus_b_shunt0)
                bus_dict["bus_b_shunt_min"].append(0.0)
                bus_dict["bus_b_shunt_max"].append(bus_b_shunt0)
                bus_dict["bus_b_dispatch"].append(0)
                bus_dict["bus_area"].append(0)

            bus_dict = dict(bus_dict)

            bus_str = tabulate(
                tabular_data=bus_dict,
                headers="keys",
                tablefmt="plain",
                disable_numparse=False,
                numalign="right",
                stralign="right",
                floatfmt=("", "", "", "6.2f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f",
                          "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "1.0f", "1.0f")
            )
            bus_str = bus_str.replace("BUS ", "BUS:")
            bus_str = bus_str.replace("bus_area", "bus_area :=")
            bus_str = "param:\n" + bus_str

            file.write(bus_str)

        with open(abspath(join(output_path, "der.txt")), "w") as file:
            der_dict = defaultdict(list)

            for bus_index, bus_name in dss.buses():
                der_dict["DER_BUS"].append(bus_index)
                der_dict["bus_p_pv0"].append(0.0)
                der_dict["bus_p_pv_min"].append(0.0)
                der_dict["bus_p_pv_max"].append(0.0)
                der_dict["bus_s_pv_min"].append(0.0)
                der_dict["bus_s_pv_max"].append(0.0)
                der_dict["bus_p_ev0"].append(0.0)
                der_dict["bus_p_ev_min"].append(0.0)
                der_dict["bus_p_ev_max"].append(0.0)
                der_dict["bus_pf_ev"].append(1.0)

            der_dict = dict(der_dict)

            der_str = tabulate(
                tabular_data=der_dict,
                headers="keys",
                tablefmt="plain",
                disable_numparse=False,
                numalign="right",
                stralign="right",
                floatfmt=("", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f")
            )
            der_str = der_str.replace("DER_BUS ", "DER_BUS:")
            der_str = der_str.replace("bus_pf_ev", "bus_pf_ev :=")
            der_str = "param:\n" + der_str

            file.write(der_str)

        with open(abspath(join(output_path, "branch.txt")), "w") as file:
            branch_dict = defaultdict(list)

            branch_index = 0

            for branch in dss.lines() + dss.transformers():
                branch_index += 1
                if type(branch).__name__ == "Line":
                    branch_c = (2 * pi * frequency * ((branch.c1 * branch.length) / 10 ** 9)) / Y_base_ohm
                    branch_dict["BRANCH"].append(branch_index)
                    branch_dict["/* bus_from"].append(dss.buses_by_name()[branch.bus1])
                    branch_dict["bus_to"].append(dss.buses_by_name()[branch.bus2])
                    branch_dict["*/ branch_type"].append(0)
                    branch_dict["branch_r"].append(
                        (branch.r1 * branch.length) / Z_base_ohm if not branch.switch else 0.0001
                    )
                    branch_dict["branch_x"].append(
                        (branch.x1 * branch.length) / Z_base_ohm if not branch.switch else 0.0
                    )
                    branch_dict["branch_bsh"].append(branch_c if not branch.switch else 0.0)
                    branch_dict["branch_tap0"].append(1.0)
                    branch_dict["branch_tap_min"].append(1.0)
                    branch_dict["branch_tap_max"].append(1.0)
                    branch_dict["branch_step_size"].append(0.00625)
                    branch_dict["branch_def0"].append(0.0)
                    branch_dict["branch_def_min"].append(0.0)
                    branch_dict["branch_def_max"].append(0.0)
                    branch_dict["branch_current_max"].append(3.0)
                else:
                    branch_dict["BRANCH"].append(branch_index)
                    branch_dict["/* bus_from"].append(dss.buses_by_name()[branch.buses[0]])
                    branch_dict["bus_to"].append(dss.buses_by_name()[branch.buses[1]])
                    branch_dict["*/ branch_type"].append(1)
                    branch_dict["branch_r"].append(sum(branch.r_pu))
                    branch_dict["branch_x"].append(branch.xhl_pu)
                    branch_dict["branch_bsh"].append(0.0)
                    branch_dict["branch_tap0"].append(branch.taps[0])
                    branch_dict["branch_tap_min"].append(branch.min_tap)
                    branch_dict["branch_tap_max"].append(branch.max_tap)
                    branch_dict["branch_step_size"].append(0.00625)
                    branch_dict["branch_def0"].append(0.0)
                    branch_dict["branch_def_min"].append(0.0)
                    branch_dict["branch_def_max"].append(0.0)
                    branch_dict["branch_current_max"].append(3.0)

            branch_dict = dict(branch_dict)

            branch_str = tabulate(
                tabular_data=branch_dict,
                headers="keys",
                tablefmt="plain",
                disable_numparse=False,
                numalign="right",
                stralign="right",
                floatfmt=("", "", "", "", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f", "8.4f",
                          "8.4f", "8.4f")
            )
            branch_str = branch_str.replace("BRANCH ", "BRANCH:")
            branch_str = branch_str.replace("branch_current_max", "branch_current_max :=")
            branch_str = "param:\n" + branch_str

            file.write(branch_str)

    Gui.popup("The converted grid is stored at:", output_path)
