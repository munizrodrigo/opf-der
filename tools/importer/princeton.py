# -*- coding: utf-8 -*-
"""
This module convert any grid available at https://vanderbei.princeton.edu/ampl/nlmodels/power/index.html to this
project modelling format.

.. codeauthor:: João Rodrigo Muniz <joao.muniz@usp.br>

Example
-------
Just run it. It will change in the future.
"""

from os.path import abspath, join
from collections import defaultdict
from numpy import loadtxt
from yaml import safe_dump
from pandas import DataFrame, set_option
import PySimpleGUI as Gui


set_option("display.max_columns", None)
set_option("display.max_rows", None)
set_option("display.width", None)


if __name__ == "__main__":
    title = "OPF-DER - Princeton Importer"

    grid_bus_path = Gui.popup_get_file(
        message="Enter the original grid \".bus\" file:",
        title=title
    )

    grid_branch_path = Gui.popup_get_file(
        message="Enter the original grid \".branch\" file:",
        title=title
    )

    try:
        S_base_mva = float(Gui.popup_get_text(message="Insert the grid base MVA:", title=title))
    except ValueError:
        Gui.popup_error(
            "The specified base MVA was not a valid number. The default value of 100 MVA will be used instead!",
            title=title
        )
        S_base_mva = 100

    output_path = Gui.popup_get_folder(
        message="Enter the desired output directory:",
        title=title
    )

    output_filename = Gui.popup_get_text(
        message="Enter the desired output filename (without extension):",
        title=title
    )

    if grid_bus_path == "" or grid_branch_path == "" or output_path == "":
        Gui.popup_error(
            "One of the needed path is missing. Exiting now...",
            title=title
        )
        exit()

    grid_bus_path = abspath(grid_bus_path)
    grid_branch_path = abspath(grid_branch_path)
    output_path = abspath(output_path)

    bus_table = loadtxt(grid_bus_path, dtype=str)

    bus_dict = defaultdict(list)

    for bus in bus_table:
        bus_dict["bus"].append(int(bus[0]))
        bus_dict["bus_name"].append(str(bus[2]).split("\"")[1])
        bus_dict["bus_coords"].append([0.0, 0.0])
        bus_dict["bus_type"].append(int(bus[1]))
        bus_dict["bus_angle0"].append(float(bus[4]))
        bus_dict["bus_voltage0"].append(float(bus[3]))
        bus_dict["bus_voltage_min"].append(0.95)
        bus_dict["bus_voltage_max"].append(1.05)
        bus_dict["bus_p_gen"].append(float(bus[5]) / S_base_mva)
        bus_dict["bus_p_gen_min"].append(0.9)
        bus_dict["bus_p_gen_max"].append(1.1)
        bus_dict["bus_q_gen"].append(float(bus[6]) / S_base_mva)
        bus_dict["bus_q_gen_min"].append(float(bus[7]) / S_base_mva)
        bus_dict["bus_q_gen_max"].append(float(bus[8]) / S_base_mva)
        bus_dict["bus_p_load"].append(float(bus[9]) / S_base_mva)
        bus_dict["bus_q_load"].append(float(bus[10]) / S_base_mva)
        bus_dict["bus_g_shunt"].append(float(bus[11]))
        bus_dict["bus_b_shunt0"].append(float(bus[12]))
        bus_dict["bus_b_shunt_min"].append(float(bus[13]))
        bus_dict["bus_b_shunt_max"].append(float(bus[14]))
        bus_dict["bus_b_dispatch"].append(int(bus[15]))
        bus_dict["bus_area"].append(int(bus[16]))

    bus_dict = dict(bus_dict)
    bus_df = DataFrame.from_dict(bus_dict, orient="columns")
    bus_df = bus_df.set_index(["bus"])
    bus_dict = bus_df.to_dict(orient="index")

    branch_table = loadtxt(grid_branch_path, dtype=str)

    branch_dict = defaultdict(list)

    for branch in branch_table:
        branch_dict["branch"].append(int(branch[0]))
        branch_dict["bus_from"].append(int(branch[1]))
        branch_dict["bus_to"].append(int(branch[2]))
        branch_dict["branch_type"].append(int(branch[3]))
        branch_dict["branch_r"].append(float(branch[4]))
        branch_dict["branch_x"].append(float(branch[5]))
        branch_dict["branch_bsh"].append(float(branch[6]))
        branch_dict["branch_tap0"].append(float(branch[7]))
        branch_dict["branch_tap_min"].append(float(branch[8]))
        branch_dict["branch_tap_max"].append(float(branch[9]))
        branch_dict["branch_step_size"].append(0.00625)
        branch_dict["branch_def0"].append(float(branch[10]))
        branch_dict["branch_def_min"].append(float(branch[11]))
        branch_dict["branch_def_max"].append(float(branch[12]))
        branch_dict["branch_current_max"].append(3.0)

    branch_dict = dict(branch_dict)
    branch_df = DataFrame.from_dict(branch_dict, orient="columns")
    branch_df = branch_df.set_index(["branch"])
    branch_dict = branch_df.to_dict(orient="index")

    grid_dict = {
        "bus": bus_dict,
        "branch": branch_dict
    }

    with open(abspath(join(output_path, f"{output_filename}.yml")), "w") as file:
        safe_dump(grid_dict, file, indent=2, sort_keys=False)

    Gui.popup("The converted grid is stored at:", output_path)
