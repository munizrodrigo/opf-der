# ----------- FPO-DS MODEL ----------- #

# ---------------- Grid ------------------ #

param grid_name symbolic; # name of the grid

param n_branch default Infinity; # number of branches

set BUS; # set of buses
set BRANCH within {1..n_branch} cross BUS cross BUS; # set of branches

# ---------------- Files ----------------- #

param grid_out_filename default "pf_out.txt" symbolic;
model "ampl\\model\\grid_files_param.mod";

# -------------- Constants --------------- #

param pi := 4 * atan(1);

param total_control_adjustment = 0;

# --------------- Bus Data --------------- #

model "ampl\\model\\bus_param.mod";

# ------------- Branch Data -------------- #

model "ampl\\model\\branch_param.mod";

# -------------- Variables --------------- #

var bus_voltage {i in BUS};
var bus_angle {i in BUS};
var bus_b_shunt {i in BUS};
var branch_tap {(l,k,m) in BRANCH};
var branch_def {(l,k,m) in BRANCH};

# --------- Auxiliar Variables ----------- #

var p_g {BUS}; # final active power generation
var q_g {BUS}; # final reactive power generation

var p_d {BRANCH}; # final active direct flow
var q_d {BRANCH}; # final reactive direct flow
var p_r {BRANCH}; # final active reverse flow
var q_r {BRANCH}; # final reactive reverse flow

# ----------- System Matrix -------------- #

model "ampl\\model\\system_matrix.mod";

# ------ Auxiliar Generation Data -------- #

# TODO verify inductive and capacitive generation equations

var reactive_generation =  sum {k in BUS} abs(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))));

var inductive_generation = sum {k in BUS} min(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))), 0);

var capacitive_generation =  sum {k in BUS} max(bus_q_load[k] + sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k]-bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))), 0);

# --------------- Losses ----------------- #

var total_loss = sum {(l,k,m) in BRANCH} (branch_g[l,k,m] * (bus_voltage[k]^2 * branch_tap[l,k,m]^2 + bus_voltage[m]^2 - 2 * bus_voltage[k] * bus_voltage[m] * branch_tap[l,k,m] * cos(bus_angle[k] - bus_angle[m])));

# ------------- Constraints -------------- #

subject to p_load {k in BUS: bus_type[k] == 0 || bus_type[k] == 2}: bus_p_gen[k] - bus_p_load[k] - sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * cos(bus_angle[k] - bus_angle[m]) + B[k,m] * sin(bus_angle[k] - bus_angle[m]))) = 0;

subject to q_load {k in BUS: bus_type[k] == 0}: bus_q_gen[k] - bus_q_load[k] - sum {(k,m) in YBUS} (bus_voltage[k] * bus_voltage[m] * (G[k,m] * sin(bus_angle[k] - bus_angle[m]) - B[k,m] * cos(bus_angle[k] - bus_angle[m]))) = 0;
