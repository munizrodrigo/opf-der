# ------------- OPF-TS MODEL ------------- #

# ---------------- Grid ------------------ #

param grid_name symbolic; # name of the grid

param n_branch default Infinity; # number of branches

set BUS; # set of buses
set BRANCH within {1..n_branch} cross BUS cross BUS; # set of branches

# ---------------- Files ----------------- #

param grid_out_filename default "dopf_misocp.txt" symbolic;
model "ampl\\model\\grid_files_param.mod";

# -------------- Constants --------------- #

param pi := 4 * atan(1);

# -------- Objectives Parameters --------- #

param w default 0.0;

# --------------- Bus Data --------------- #

model "ampl\\model\\bus_param.mod";

# ------------- Branch Data -------------- #

model "ampl\\model\\branch_param.mod";

# ------------ Special Sets -------------- #

set BUS_LOAD = {i in BUS: bus_p_load[i] != 0 or bus_q_load[i] != 0};
set BRANCH_LTC = {(l,k,m) in BRANCH: branch_type[l,k,m] == 2};

# -------------- Variables --------------- #

var bus_p_g {i in BUS} >= bus_p_gen_min[i], <= bus_p_gen_max[i];
var bus_p_c {i in BUS} >= bus_p_load[i], <= bus_p_load[i];
var bus_q_g {i in BUS} >= bus_q_gen_min[i], <= bus_q_gen_max[i];
var bus_q_c {i in BUS} >= bus_q_load[i], <= bus_q_load[i];;
var bus_p_inj {i in BUS} = bus_p_g[i] - bus_p_c[i];
var bus_q_inj {i in BUS} = bus_q_g[i] - bus_q_c[i];
var bus_voltage_square {i in BUS};
var branch_current_square {(l,i,j) in BRANCH};
var branch_p {(l,i,j) in BRANCH};
var branch_q {(l,i,j) in BRANCH};
var branch_tap {(l,i,j) in BRANCH_LTC} >= round((branch_tap_min[l,i,j] - 1) / branch_step_size[l,i,j]), <= round((branch_tap_max[l,i,j] - 1) / branch_step_size[l,i,j]) integer;
var branch_ltc_k1 {(l,i,j) in BRANCH_LTC} = 1 + branch_step_size[l,i,j] * branch_tap[l,i,j];
var branch_ltc_k2 {(l,i,j) in BRANCH_LTC};

var w_mccormick {(l,i,j) in BRANCH_LTC};

param bus_voltage_square_mccormick_upper {i in BUS} default bus_voltage_max[i]^2;
param bus_voltage_square_mccormick_lower {i in BUS} default bus_voltage_min[i]^2;

param branch_ltc_k2_mccormick_upper {(l,i,j) in BRANCH_LTC} default 1 / bus_voltage_min[j]^2;
param branch_ltc_k2_mccormick_lower {(l,i,j) in BRANCH_LTC} default 1 / bus_voltage_max[j]^2;

# --------------- Losses ----------------- #

var total_loss = sum {(l,k,m) in BRANCH} branch_r[l,k,m] * branch_current_square[l,k,m];

# ------------- Objectives --------------- #

minimize losses: total_loss;

# ------------- Constraints -------------- #

subject to p_inj {j in BUS}: bus_p_inj[j] = ( sum {(l,i,k) in BRANCH: i == j} branch_p[l,i,k] ) - ( sum {(l,i,k) in BRANCH: k == j} (branch_p[l,i,k] - branch_r[l,i,k] * branch_current_square[l,i,k]) ) + bus_g_shunt[j] * bus_voltage_square[j];

subject to q_inj {j in BUS}: bus_q_inj[j] = ( sum {(l,i,k) in BRANCH: i == j} branch_q[l,i,k] ) - ( sum {(l,i,k) in BRANCH: k == j} (branch_q[l,i,k] - branch_x[l,i,k] * branch_current_square[l,i,k]) ) + bus_b_shunt0[j] * bus_voltage_square[j];

subject to voltage_drop {(l,i,j) in BRANCH}: bus_voltage_square[j] = bus_voltage_square[i] - 2 * (branch_r[l,i,j] * branch_p[l,i,j] + branch_x[l,i,j] * branch_q[l,i,j]) + (branch_r[l,i,j]^2 + branch_x[l,i,j]^2) * branch_current_square[l,i,j]^2;

subject to power_cone {(l,i,j) in BRANCH}: sqrt(1e-12 + 4 * branch_p[l,i,j]^2 + 4 * branch_q[l,i,j]^2 + (branch_current_square[l,i,j] - bus_voltage_square[i])^2) <= branch_current_square[l,i,j] + bus_voltage_square[i];

subject to branch_ltc_k1_cone {(l,i,j) in BRANCH_LTC}: sqrt(1e-12 + 4 * branch_ltc_k1[l,i,j]^2 + (bus_voltage_square[j] - branch_ltc_k2[l,i,j])^2) <= bus_voltage_square[j] + branch_ltc_k2[l,i,j];

subject to branch_ltc_k2_cone {(l,i,j) in BRANCH_LTC}: sqrt(4 + (branch_ltc_k2[l,i,j] - bus_voltage_square[i])^2) <= branch_ltc_k2[l,i,j] + bus_voltage_square[i];

subject to bus_voltage_square_loads {i in BUS_LOAD}: bus_voltage_min[i]^2 <= bus_voltage_square[i] <= bus_voltage_max[i]^2;

subject to mccormick_upper_1 {(l,i,j) in BRANCH_LTC}: w_mccormick[l,i,j] >= branch_ltc_k2_mccormick_lower[l,i,j] * bus_voltage_square[i] + bus_voltage_square_mccormick_lower[i] * branch_ltc_k2[l,i,j] - branch_ltc_k2_mccormick_lower[l,i,j] * bus_voltage_square_mccormick_lower[i];

subject to mccormick_upper_2 {(l,i,j) in BRANCH_LTC}: w_mccormick[l,i,j] >= branch_ltc_k2_mccormick_upper[l,i,j] * bus_voltage_square[i] + bus_voltage_square_mccormick_upper[i] * branch_ltc_k2[l,i,j] - branch_ltc_k2_mccormick_upper[l,i,j] * bus_voltage_square_mccormick_upper[i];

subject to mccormick_lower_1 {(l,i,j) in BRANCH_LTC}: w_mccormick[l,i,j] <= branch_ltc_k2_mccormick_lower[l,i,j] * bus_voltage_square[i] + bus_voltage_square_mccormick_upper[i] * branch_ltc_k2[l,i,j] - branch_ltc_k2_mccormick_lower[l,i,j] * bus_voltage_square_mccormick_upper[i];

subject to mccormick_lower_2 {(l,i,j) in BRANCH_LTC}: w_mccormick[l,i,j] <= bus_voltage_square_mccormick_lower[i] * branch_ltc_k2[l,i,j] + branch_ltc_k2_mccormick_upper[l,i,j] * bus_voltage_square[i] - bus_voltage_square_mccormick_lower[i] * branch_ltc_k2_mccormick_upper[l,i,j];
