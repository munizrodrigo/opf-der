from os.path import abspath
from time import time
from amplpy import AMPL, OutputHandler, ErrorHandler
from tabulate import tabulate


class AMPLOutput(OutputHandler):
    def output(self, kind, msg):
        pass


class AMPLError(ErrorHandler):
    def error(self, exception):
        print("Error:", exception.get_message())

    def warning(self, exception):
        print("Warning:", exception.get_message())


def main():
    ampl.option["solver"] = "bonmin"
    ampl.set_option("bonmin_options", "bonmin.bb_log_level 0 bonmin.nlp_log_level 0")

    ampl.read("opf_ts.mod")
    ampl.read_data("opf_ts.dat")

    ampl.eval("objective losses;")
    ampl.solve()

    ampl.param["min_total_loss"] = ampl.var["total_loss"].value()
    ampl.param["max_total_control_adjustment"] = ampl.var["total_control_adjustment"].value()

    ampl.eval("objective control_adjustment;")
    ampl.solve()

    ampl.param["max_total_loss"] = ampl.var["total_loss"].value()
    ampl.param["min_total_control_adjustment"] = ampl.var["total_control_adjustment"].value()

    ampl.eval("objective losses_and_control;")
    ampl.solve()

    weight = [i / 10 for i in range(0, 11)]
    losses = []
    n_control = []
    solve_time = []

    default_grid_out_file = str(ampl.param["grid_out_file"].value())

    for w in weight:
        ampl.param["w"] = w

        start = time()

        ampl.solve_async()
        ampl.wait()

        solve_time.append(time() - start)

        losses.append(ampl.var["total_loss"].value())
        n_control.append(int(ampl.var["total_control_adjustment"].value()))

        ampl.eval("commands \"ampl\\script\\calculate_power.run\";")
        ampl.eval("commands \"ampl\\script\\calculate_flow.run\";")

        grid_out_file = str(w).replace(".", "_")
        grid_out_file = default_grid_out_file.replace("out.txt", f"w{grid_out_file}_out.txt")
        ampl.param["grid_out_file"] = grid_out_file

        ampl.eval("commands \"ampl\\script\\output_file.run\";")

    data = {
        "w": weight,
        "time": solve_time,
        "losses": losses,
        "n": n_control
    }

    out_path = abspath(default_grid_out_file)

    with open(out_path, "w") as out_file:
        out_file.write(tabulate(data, headers="keys", tablefmt="github"))

    out_path = str(abspath(default_grid_out_file)).replace(".txt", ".md")

    with open(out_path, "w") as out_file:
        out_file.write(tabulate(data, headers="keys", tablefmt="github"))


if __name__ == "__main__":
    try:
        ampl = AMPL()

        output_handler = AMPLOutput()
        ampl.set_output_handler(output_handler)

        error_handler = AMPLError()
        ampl.set_error_handler(error_handler)

        main()

    except Exception as exc:
        raise exc
