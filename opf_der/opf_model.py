from math import sqrt, degrees
from os.path import join
from numpy import angle, conjugate
from amplpy import AMPL, DataFrame as AMPLFrame
from pandas import DataFrame as PandasFrame, set_option
from .grid import Grid


set_option("display.max_columns", None)
set_option("display.max_rows", None)
set_option("display.width", None)


class OPFModel(object):
    def __init__(self, ampl: AMPL, model_name: str):
        self.ampl = ampl
        self.model_name = model_name

    def export_model(self, path=""):
        if not path:
            self.ampl.export_model(f"{self.model_name}.mod")
        else:
            self.ampl.export_model(join(path, f"{self.model_name}.mod"))

    def export_data(self, path=""):
        if not path:
            self.ampl.export_data(f"{self.model_name}.dat")
        else:
            self.ampl.export_data(join(path, f"{self.model_name}.dat"))


class DOPFCR(OPFModel):
    def __init__(self, ampl, bus_df: AMPLFrame, branch_df: AMPLFrame, grid: Grid, solver="minos"):
        super(DOPFCR, self).__init__(ampl=ampl, model_name="dopf_cr_farivar")
        self.ampl.option["solver"] = solver
        self.bus_df = bus_df
        self.branch_df = branch_df
        self.grid = grid

    def generate_model(self):
        self.ampl.eval("set BUS;")
        self.ampl.eval("set BRANCH within {BUS, BUS};")

        for key in self.bus_df.get_headers()[1:]:
            param_cmd = f"param {key} {{BUS}}"
            if self.bus_df.to_pandas()[key].dtype == "object":
                param_cmd += " symbolic"
            param_cmd += ";"
            self.ampl.eval(param_cmd)

        for key in self.branch_df.get_headers()[2:]:
            self.ampl.eval(f"param {key} {{BRANCH}};")

        self.ampl.eval("param r{(i,j) in BRANCH} = branch_r[i,j];")
        self.ampl.eval("param x{(i,j) in BRANCH} = branch_x[i,j];")
        self.ampl.eval("param g{(i,j) in BRANCH} = r[i,j] / (r[i,j]^2 + x[i,j]^2);")
        self.ampl.eval("param b{(i,j) in BRANCH} = x[i,j] / (r[i,j]^2 + x[i,j]^2);")

        self.ampl.eval("param g_sh{i in BUS} = bus_g_shunt[i];")
        self.ampl.eval("param b_sh{i in BUS} = bus_b_shunt0[i];")

        self.ampl.eval("param pg_max {i in BUS} = bus_p_gen_max[i];")
        self.ampl.eval("param pg_min {i in BUS} = bus_p_gen_min[i];")
        self.ampl.eval("var pg {i in BUS} <= pg_max[i], >= pg_min[i];")

        self.ampl.eval("param pc_max {i in BUS} = bus_p_load[i];")
        self.ampl.eval("param pc_min {i in BUS} = bus_p_load[i];")
        self.ampl.eval("var pc {i in BUS} <= pc_max[i], >= pc_min[i];")

        self.ampl.eval("var p {i in BUS} = pg[i] - pc[i];")

        self.ampl.eval("param qg_max {i in BUS} = bus_q_gen_max[i];")
        self.ampl.eval("param qg_min {i in BUS} = bus_q_gen_min[i];")
        self.ampl.eval("var qg {i in BUS} <= qg_max[i], >= qg_min[i];")

        self.ampl.eval("param qc_max {i in BUS} = bus_q_load[i];")
        self.ampl.eval("param qc_min {i in BUS} = bus_q_load[i];")
        self.ampl.eval("var qc {i in BUS} <= qc_max[i], >= qc_min[i];")

        self.ampl.eval("var q {i in BUS} = qg[i] - qc[i];")

        self.ampl.eval("var P {BRANCH};")
        self.ampl.eval("var Q {BRANCH};")

        self.ampl.eval("var v {i in BUS} >= bus_voltage_min[i]^2, <= bus_voltage_max[i]^2;")

        self.ampl.eval("var l{(i,j) in BRANCH} <= branch_current_max[i,j]^2;")

        self.ampl.eval("minimize losses: sum {(i,j) in BRANCH} branch_r[i,j] * l[i,j];")

        self.ampl.eval("subject to pj {j in BUS}: p[j] = sum {(i,k) in BRANCH: i == j} (P[i,k]) "
                       "- sum {(i,k) in BRANCH: k == j} (P[i,k] - r[i,k] * l[i,k]) + (g_sh[j] * v[j]);")
        self.ampl.eval("subject to qj {j in BUS}: q[j] = sum {(i,k) in BRANCH: i == j} (Q[i,k]) "
                       "- sum {(i,k) in BRANCH: k == j} (Q[i,k] - x[i,k] * l[i,k]) + (b_sh[j] * v[j]);")

        self.ampl.eval("subject to vj {(i,j) in BRANCH}: v[j] = v[i] - 2 * (r[i,j] * P[i,j] + x[i,j] * Q[i,j]) "
                       "+ (r[i,j]^2 + x[i,j]^2) * l[i,j];")

        self.ampl.eval("subject to lij {(i,j) in BRANCH}: l[i,j] >= (P[i,j]^2 + Q[i,j]^2) / v[i];")

    def set_data(self):
        self.ampl.set_data(self.bus_df, "BUS")
        self.ampl.set_data(self.branch_df, "BRANCH")

        self.ampl.eval("for {i in BUS} {let v[i] := bus_voltage0[i]^2;};")
        self.ampl.eval("fix {i in BUS: bus_type[i] == 3} v[i];")

    def solve(self):
        self.ampl.solve()

    def get_params(self, pandas=True):
        params = self.ampl.get_parameters()
        params_dict = {}
        for param in params:
            params_dict[param[0]] = param[1].get_values() if not pandas else param[1].get_values().to_pandas()
        return params_dict

    def get_variables(self, pandas=True):
        variables = self.ampl.get_variables()
        var_dict = {}
        for var in variables:
            var_dict[var[0]] = var[1].get_values() if not pandas else var[1].get_values().to_pandas()
        return var_dict

    def recover_complex_power(self):
        var_dict = self.get_variables()

        p = var_dict["P"].to_dict(orient="index")
        q = var_dict["Q"].to_dict(orient="index")

        s = {}
        for index in p.keys():
            s[index] = {"S.val": complex(p[index]["P.val"], q[index]["Q.val"])}
        var_dict["S"] = PandasFrame.from_dict(s, orient="index")

        return var_dict

    def recover_impedance(self):
        param_dict = self.get_params()

        r = param_dict["r"].to_dict(orient="index")
        x = param_dict["x"].to_dict(orient="index")

        z = {}
        for index in r.keys():
            z[index] = {"z": complex(r[index]["r"], x[index]["x"])}
        param_dict["z"] = PandasFrame.from_dict(z, orient="index")

        return param_dict

    def recover_angle(self):
        var_dict = self.recover_complex_power()
        param_dict = self.recover_impedance()

        v_dict = var_dict["v"].to_dict(orient="index")
        s_dict = var_dict["S"].to_dict(orient="index")
        z_dict = param_dict["z"].to_dict(orient="index")

        temp_graph = self.grid.directed_graph.copy()

        for node in self.grid.iter_spanning_tree():
            j = int(node)

            temp_graph.nodes[node]["v"] = v_dict[j]["v.val"]
            temp_graph.nodes[node]["V"] = sqrt(v_dict[j]["v.val"])
            if j == 1:
                temp_graph.nodes[node]["theta_V"] = 0.0

            vj = temp_graph.nodes[node]["v"]
            theta_Vj = temp_graph.nodes[node]["theta_V"]

            for neighbor in self.grid.directed_graph.neighbors(node):
                k = int(neighbor)

                temp_graph[node][neighbor]["S"] = s_dict[(j, k)]["S.val"]
                temp_graph[node][neighbor]["z"] = z_dict[(j, k)]["z"]

                Sjk = temp_graph[node][neighbor]["S"]
                zjk = temp_graph[node][neighbor]["z"]

                theta_Ijk = theta_Vj - angle(Sjk)

                temp_graph[node][neighbor]["theta_I"] = theta_Ijk

                theta_Vk = theta_Vj - angle(vj - conjugate(zjk) * Sjk)

                temp_graph.nodes[neighbor]["theta_V"] = theta_Vk

        self.grid.solved_directed_graph = temp_graph

    def recover_variables(self):
        var_dict = self.get_variables()
        self.recover_angle()

        p = var_dict["p"].to_dict(orient="index")
        pc = var_dict["pc"].to_dict(orient="index")
        pg = var_dict["pg"].to_dict(orient="index")

        q = var_dict["q"].to_dict(orient="index")
        qc = var_dict["qc"].to_dict(orient="index")
        qg = var_dict["qg"].to_dict(orient="index")

        for node in self.grid.solved_directed_graph.nodes():
            j = int(node)

            del self.grid.solved_directed_graph.nodes[node]["p_load"]
            del self.grid.solved_directed_graph.nodes[node]["q_load"]

            self.grid.solved_directed_graph.nodes[node]["p"] = p[j]["p.val"]
            self.grid.solved_directed_graph.nodes[node]["pc"] = pc[j]["pc.val"]
            self.grid.solved_directed_graph.nodes[node]["pg"] = pg[j]["pg.val"]

            self.grid.solved_directed_graph.nodes[node]["q"] = q[j]["q.val"]
            self.grid.solved_directed_graph.nodes[node]["qc"] = qc[j]["qc.val"]
            self.grid.solved_directed_graph.nodes[node]["qg"] = qg[j]["qg.val"]

        P = var_dict["P"].to_dict(orient="index")
        Q = var_dict["Q"].to_dict(orient="index")
        l = var_dict["l"].to_dict(orient="index")

        for edge in self.grid.solved_directed_graph.edges():
            j = int(edge[0])
            k = int(edge[1])
            self.grid.solved_directed_graph.edges[edge]["P"] = P[(j, k)]["P.val"]
            self.grid.solved_directed_graph.edges[edge]["Q"] = Q[(j, k)]["Q.val"]
            self.grid.solved_directed_graph.edges[edge]["l"] = l[(j, k)]["l.val"]
            self.grid.solved_directed_graph.edges[edge]["I"] = sqrt(l[(j, k)]["l.val"])
            self.grid.solved_directed_graph.edges[edge]["S"] = abs(self.grid.solved_directed_graph.edges[edge]["S"])
            self.grid.solved_directed_graph.edges[edge]["z"] = abs(self.grid.solved_directed_graph.edges[edge]["z"])

    def get_output_dataframe(self):
        self.recover_variables()

        bus_dict = {}
        for node in self.grid.solved_directed_graph.nodes():
            temp_dict = dict(self.grid.solved_directed_graph.nodes[node])
            del temp_dict["pos"]
            del temp_dict["v"]
            bus_dict[node] = temp_dict

        bus_df = PandasFrame.from_dict(bus_dict, orient="index")
        bus_df["theta_V"] = bus_df["theta_V"].map(lambda value: degrees(value))
        # bus_df.rename(columns={"theta_V": "\u03B8V"}, inplace=True)

        branch_dict = {}
        for edge in self.grid.solved_directed_graph.edges():
            temp_dict = dict(self.grid.solved_directed_graph.edges[edge])
            del temp_dict["l"]
            branch_dict[edge] = temp_dict

        branch_df = PandasFrame.from_dict(branch_dict, orient="index")
        branch_df = branch_df.reindex(columns=["r", "x", "b", "z", "I", "theta_I", "P", "Q", "S"])
        branch_df["theta_I"] = branch_df["theta_I"].map(lambda value: degrees(value))
        branch_df["rI^2"] = branch_df["r"] * branch_df["I"] ** 2
        # branch_df.rename(columns={"theta_I": "\u03B8I"}, inplace=True)

        return bus_df, branch_df
