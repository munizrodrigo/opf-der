import pypsa
import matplotlib.pyplot as plt
from networkx import minimum_spanning_tree, bfs_tree, draw, Graph, DiGraph


class Grid(object):
    def __init__(self, grid_dict):
        self.grid_dict = grid_dict
        self.pypsa_network = self.generate_pypsa_network()
        self.graph, self.directed_graph = self.generate_networkx_graphs()
        self.spanning_tree = self.get_minimum_spanning_tree()
        self.root = self.find_slack()
        self.solved_directed_graph = None

    def create_ampl_bus_dict(self):
        bus_dict = {}
        for key, value in self.grid_dict["bus"].items():
            del value["bus_coords"]
            bus_dict[key] = value
        return bus_dict

    def create_ampl_branch_dict(self):
        branch_dict = {}
        for key, value in self.grid_dict["branch"].items():
            branch_dict[key] = value
        return branch_dict

    def generate_pypsa_network(self):
        network = pypsa.Network()

        for bus_index, bus_attr in self.grid_dict["bus"].items():
            network.add(
                "Bus",
                name=bus_index,
                x=bus_attr["bus_coords"][0],
                y=bus_attr["bus_coords"][1],
                v_mag_pu_set=bus_attr["bus_voltage0"],
                v_mag_pu_min=bus_attr["bus_voltage_min"],
                v_mag_pu_max=bus_attr["bus_voltage_max"],
            )

            if bus_attr["bus_p_load"] != 0.0 or bus_attr["bus_q_load"] != 0.0:
                network.add(
                    "Load",
                    name=f"L_{bus_index}",
                    bus=bus_index,
                    p_set=bus_attr["bus_p_load"],
                    q_set=bus_attr["bus_q_load"]
                )

            if bus_attr["bus_p_gen"] > 0.0 or bus_attr["bus_p_gen_max"] > 0.0:
                if bus_attr["bus_type"] == 3:
                    control = "Slack"
                elif bus_attr["bus_type"] == 2:
                    control = "PV"
                else:
                    control = "PQ"
                network.add(
                    "Generator",
                    name=f"G_{bus_index}",
                    bus=bus_index,
                    control=control,
                    p_nom=bus_attr["bus_p_gen"],
                    p_min_pu=bus_attr["bus_p_gen_min"],
                    p_max_pu=bus_attr["bus_p_gen_max"],
                    p_set=bus_attr["bus_p_gen"],
                    q_set=bus_attr["bus_q_gen"],
                )

        for branch_index, branch_attr in self.grid_dict["branch"].items():
            network.add(
                "Line",
                name=branch_index,
                bus0=branch_attr["bus_from"],
                bus1=branch_attr["bus_to"],
                r=branch_attr["branch_r"],
                x=branch_attr["branch_x"],
                b=branch_attr["branch_bsh"],
            )

        return network

    def find_slack(self):
        for bus_name, bus_attr in self.grid_dict["bus"].items():
            if bus_attr["bus_type"] == 3:
                return bus_name

    def generate_networkx_graphs(self):
        graph = Graph()
        directed_graph = DiGraph()

        for bus_index, bus_attr in self.grid_dict["bus"].items():
            if bus_attr["bus_type"] == 3:
                control = "Slack"
            elif bus_attr["bus_type"] == 2:
                control = "PV"
            else:
                control = "PQ"

            graph.add_node(
                node_for_adding=bus_index,
                bus_name=bus_attr["bus_name"],
                pos=[bus_attr["bus_coords"][0], bus_attr["bus_coords"][1]],
                p_load=bus_attr["bus_p_load"],
                q_load=bus_attr["bus_q_load"],
                type=control
            )

            directed_graph.add_node(
                node_for_adding=bus_index,
                bus_name=bus_attr["bus_name"],
                pos=[bus_attr["bus_coords"][0], bus_attr["bus_coords"][1]],
                p_load=bus_attr["bus_p_load"],
                q_load=bus_attr["bus_q_load"],
                type=control
            )

        for branch_index, branch_attr in self.grid_dict["branch"].items():
            graph.add_edge(
                u_of_edge=branch_attr["bus_from"],
                v_of_edge=branch_attr["bus_to"],
                r=branch_attr["branch_r"],
                x=branch_attr["branch_x"],
                b=branch_attr["branch_bsh"]
            )

            directed_graph.add_edge(
                u_of_edge=branch_attr["bus_from"],
                v_of_edge=branch_attr["bus_to"],
                r=branch_attr["branch_r"],
                x=branch_attr["branch_x"],
                b=branch_attr["branch_bsh"]
            )

        return graph, directed_graph

    def get_minimum_spanning_tree(self):
        tree = minimum_spanning_tree(G=self.graph, weight="r")
        return tree

    def iter_spanning_tree(self, root=None):
        if root is None:
            root = self.root
        return bfs_tree(self.graph, source=root)

    def get_pos(self):
        pos = {}
        for node, attrs in self.graph.nodes.items():
            pos[node] = tuple(attrs["pos"])
        return pos

    def basic_draw(self):
        draw(self.graph, pos=self.get_pos())
        plt.show()
