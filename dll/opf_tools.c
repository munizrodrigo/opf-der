/****************************************************************
Author: João Rodrigo Muniz

This is a compilation of auxiliary functions to be used on AMPL.
It is focused on optimal power flow problem and makes this
modeling easier.

Useful Links:

https://ampl.com/netlib/ampl/solvers/funclink/

https://ampl.com/netlib/ampl/solvers/index.html

****************************************************************/

#include "funcadd.h"

/* Normalize function between the minimum and maximum values
 * Example:
 * normalize(f, f_min, f_max)
 */
static real normalize(arglist *al)
{
	real f = al->ra[0];
	real f_min = al->ra[1];
	real f_max = al->ra[2];
	real f_normalized = (f - f_min) / (f_max - f_min);
	return f_normalized;
}

/* Necessary to add functions on AMPL */
void funcadd(AmplExports *ae){
	/* Insert calls on addfunc here... */

    /* Arg 3, called type, must satisfy 0 <= type <= 6:
     * type&1 == 0:	0,2,4,6	==> force all arguments to be numeric.
     * type&1 == 1:	1,3,5	==> pass both symbolic and numeric arguments.
     * type&6 == 0:	0,1	==> the function is real valued.
     * type&6 == 2:	2,3	==> the function is char * valued; static storage
                    suffices: AMPL copies the return value.
     * type&6 == 4:	4,5	==> the function is random (real valued).
     * type&6 == 6: 6	==> random, real valued, pass nargs real args,
     *				0 <= nargs <= 2.
     *
     * Arg 4, called nargs, is interpretted as follows:
     *	>=  0 ==> the function has exactly nargs arguments
     *	<= -1 ==> the function has >= -(nargs+1) arguments.
     *
     * Arg 5, called funcinfo, is copied without change to the arglist
     *	structure passed to the function; funcinfo is for the
     *	function to use or ignore as it sees fit.
     */

    addfunc("normalize", (rfunc)normalize, 0, 3, 0);
}