from os import remove
from os.path import dirname, abspath, join
from subprocess import check_output
from shutil import copyfile
from compile import run_scons


if __name__ == "__main__":
    run_scons()
    ampl_path = abspath(dirname(bytes(check_output(["where", "ampl"])).decode("utf-8")))
    copyfile(src="opf_tools.dll", dst=abspath(join(ampl_path, "opf_tools.dll")))
    try:
        remove("opf_tools.dll")
    except FileNotFoundError:
        pass
