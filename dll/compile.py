from os import remove
from subprocess import call


def run_scons():
    call(["scons"])

    try:
        remove(".sconsign.dblite")
        remove("opf_tools.exp")
        remove("opf_tools.obj")
        remove("opf_tools.lib")
    except FileNotFoundError:
        pass


if __name__ == "__main__":
    run_scons()
